\documentclass[12pt,a4paper]{article}
\usepackage[utf8x]{inputenc}
\usepackage{ucs}
\usepackage{amsmath,amsthm}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\newtheorem{theorem}{Theorem}
\author{Andreas Söderlund \and Jacob Wikmark}
\title{Homework 5 \\ NUMA12}
\begin{document}
\maketitle
\section*{Task 1}
We wish to find the best $L_1$ approximation from $\mathcal{P}_2$ to $f(x) = \sin x + \cos x$ for $x\in [\pi/3,5\pi/3]$, that is, the function $p^\ast = ax^2 + bx + c$ that minimizes
\[
\| f(x) - p^\ast(x)\|_1 = \int\limits_{\pi/3}^{5\pi/3}|f(x) - p^\ast(x)| dx
\]
We know from before that $\mathcal{P}_2$ satisfies the Haar condition, and also that $f\in\mathcal{C}[\pi/3,5\pi/3]$ because $f$ is a linear combination of continuous functions. This means that we can use theorem $14.4$ from the book if the error function
\[
e^\ast(x) = f(x) - p^\ast(x),\ x\in [\pi/3,5\pi/3]
\]
changes signs exactly $2+1=3$ times. Following the procedure laid out in the book, we assume that this is the case and that $e^\ast(x)$ changes signs exactly $3$ times. Then, from theorem $14.4$ we know that the zeros of $e^\ast(x)$ don't depend on $f$. Because $\mathcal{A} = \mathcal{P}_2$, we can use theorem $14.5$ to find the zeros exactly using the modified formula
\[
\xi_i = \frac{1}{2}(a + b) + \frac{1}{2}(b - a) \cos\Big(\frac{(n + 1 - i)\pi}{n + 2}\Big),\ i=0,1,\dots,n
\]
where $[a,b]$ is the interval. For us, we have
\[
\xi_i = \pi + \frac{2\pi}{3}\cos\Big(\frac{(3-i)\pi}{4}\Big),\ i=0,1,2
\]
which gives us $\xi_0 = \pi - \frac{\sqrt{2}\pi}{3}$, $\xi_1 = \pi$ and $\xi_2 = \pi + \frac{\sqrt{2}\pi}{3}$. Because these are the zeros of the error function we have that $f(\xi_i) = p^\ast(\xi_i)$ and we need to solve the equation system
\begin{align*}
&\left\{
\begin{array}{l l}
p^\ast(\pi - \frac{\sqrt{2}\pi}{3}) &= \cos(\pi - \frac{\sqrt{2}\pi}{3}) + \sin(\pi - \frac{\sqrt{2}\pi}{3})\\
p^\ast(\pi) &= \cos(\pi) + \sin(\pi)\\
p^\ast(\pi + \frac{\sqrt{2}\pi}{3}) &= \cos(\pi + \frac{\sqrt{2}\pi}{3}) + \sin(\pi + \frac{\sqrt{2}\pi}{3})\\
\end{array}\right. \Leftrightarrow \\
&\left\{
\begin{array}{l l}
p^\ast(\pi - \frac{\sqrt{2}\pi}{3}) &= -\cos\Big(\frac{\sqrt{2}\pi}{3}\Big) + \sin\Big(\frac{\sqrt{2}\pi}{3}\Big)\\
p^\ast(\pi) &= -1 \\
p^\ast(\pi + \frac{\sqrt{2}\pi}{3}) &= -\cos\Big(\frac{\sqrt{2}\pi}{3}\Big) - \sin\Big(\frac{\sqrt{2}\pi}{3}\Big)
\end{array}\right.
\end{align*}
which is easily solvable using numpy:s linalg.solve function. The important part of the code looks as follows:
\begin{verbatim}
coefmatr = np.array([[xi0**2, xi0, 1],[xi1**2, xi1, 1], [xi2**2, xi2, 1]])
ansarr = np.array([func0, func1, func2])

ans = np.linalg.solve(coefmatr,ansarr)
\end{verbatim}
This gives us the following polynomial: $p^\ast(x) = 0.41504039x^2 -3.28029004x + 5.20905063$. When we plotted $p^\ast(x)$ and $f(x)$ we got the following plot:
\begin{center}
\includegraphics[width=\textwidth]{images/5_1_approx.png}
\end{center}
where we easily see that the error function $e^\ast(x) = f(x) - p^\ast(x)$ changes signs exactly $3$ times on the interval $[\pi/3,5\pi/3]$. This means that the earlier assumption was correct and that $p^\ast(x)$ is indeed the best $L_1$ approximation from $\mathcal{P}_2$ to $f(x)$.
\section*{Task 2}
Due to symmetry, we can reduce this problem to finding the minimal
\begin{equation*}
\int_0^1 |x^2-p(x)|dx
\end{equation*}
where $p(x)$ is an arbitrary line. That is, 
\begin{equation*}
\int_0^1|x^2-ax-b|dx
\end{equation*}
intuitively, it is obvious that the line minimizing the expression must intersect $x^2$ twice. It should suffice to say that if it crossed it zero times, it could be moved closer to the function, and the integral would be reduced, and if it crossed the function once it could be rotated in such a way that the integral would be reduced. This leaves us with the following expression
\begin{equation*}
\begin{split}
\int_0^1|x^2-ax-b|dx =\Big| \int_0^{c_1} (x^2-ax-b)dx -\int_{c_1}^{c_2} (x^2-ax-b)dx + \int_{c_2}^{1}(x^2-ax-b)dx\Big| = \\
\Big| \Big[x^3/3 - ax^2/2 -bx\Big]_0^{c_1} - \Big[x^3/3 - ax^2/2 -bx\Big]_{c_1}^{c_2} +  \Big[x^3/3 - ax^2/2 -bx\Big]_{c_2}^{1} \Big | = \\
\Big| 2(c_1^3/3-ac_1^2/2 -bc_1) -2(c_2^3/3-ac_2^2/2-bc_2)+(1/3-a/2-b)\Big |
\end{split}
\end{equation*}
We call the above expression $e(a,b)$. It would seem that it is a function of $c_1$ and $c_2$ also, but these are actually completely determined by $a,b$. There is no need to define these relationships explicitly. Clearly, from the way it is formulated, it is not possible that $e=0$, so it should be nice and differentiable, and we should be able to use analysis to find its minima. We will ignore the absolute value signs since they will make no difference in this case. Differentiating we get
\begin{equation*}
\begin{split}
\frac{de}{da}= -c_1^2 +c_2^2-1/2 \\
\frac{de}{db}=-2c_1 +2c_2 -1 \\
\end{split}
\end{equation*} 
setting to zero and simplifying
\begin{equation*}
\begin{split}
-c_1^2+c_2^2-1/2=0 \\
-2c_1 + 2c_2-1 = 0 \\
c_2=c_1+1/2 \\
-(c_1)^2+(c_1+1/2)^2 -1/2 = c_1+1/4-1/2 = 0 \\
c_1=1/4 \\
c_2=3/4
\end{split}
\end{equation*}
So the minimizing line intersects the function at the points $(\frac{1}{4},\frac{1}{16})$ and $(\frac{3}{4},\frac{9}{16})$. The line passing through these points is $y(x)=x-3/16$.\\
This produces the resulting line segments $y=-3/16-x$ on $[-1,0]$ and $y=-3/16+x$ on $[0,1]$. These meet at the point $(0,-3/16)$.
\begin{center}
\includegraphics[width=\textwidth]{images/hw5p2.jpg}
\end{center}
\section*{Task 3}
Given an odd number of points, we will have the line minimizing the total distance measured in the described fashion crossing at least one point. If this were not the case, we would be able to move the line in the direction of the side with more points and get a lesser distance. In fact, letting $p,q,n$ denote the number of points above, below or upon the line respectively, we have that $p+n\geq q$ and $q+n\geq p$ by the above argument, or equivalently, $p-n\leq q\leq p+n$. Knowing that it crosses at least one point, we make the following observation. The angle is bounded. That is, there is a maximum angle or a minimum angle. Increasing the angle above the maximum certainly increases the total distance, and decreasing the angle below the minimum will certainly increase the total distance. Now, we apply bruteforce calculation with a given stepsize rotating the line through the minimum angle to the maximum on each point to obtain the minimum distance. Below is python-code performing this task.
\begin{verbatim}
'Linear solver'
import matplotlib.pyplot as plt
import numpy as np
datax=[0,1,2,3,4,5,6,7,8]
datay=[-35.,-56.,0.,-16.,-3.,4.,10.,53.,69.]
stepsize=0.01

minset=[]
totmin=10000
for x in datax:
    pointmin=10000
    print x
    min=1000
    max=0
    for xp in datax:
        if x != xp:
            m=(datay[x]-datay[xp])/(datax[x]-datax[xp])
            if m<min:
                min=m
            if m>max:
                max=m
    res=int((max-min)/stepsize)
    resset=np.linspace(min,max,res)
    for r in resset:
        sum=0
        for xp in datax:
            if xp != x:
                sum+=abs(datay[xp]-datay[x]-r*(xp-x))
        if sum<pointmin:
            pointmin=sum
            pointx=x
            pointr=r
    minset.append(pointmin)
    if pointmin<totmin:
        totmin=pointmin
        xval=pointx
        rval=pointr
print 'MINIMAL TOTAL DISTANCE ALL POINTS'
print totmin
print 'MINIMAL TOTAL DISTANCE PER POINT'
print minset
print 'POINT PRODUCING MINIMAL TOTAL DISTANCE'
print xval
print 'DERIVATIVE OF LINE'
print rval
xplot=np.linspace(0,8)
yplot=[datay[xval]+rval*(x-xval) for x in xplot]
plt.plot(datax,datay,'o',xplot,yplot)
plt.show()

\end{verbatim}
And the result yielded:
\begin{verbatim}
MINIMAL TOTAL DISTANCE ALL POINTS
115.000833565
MINIMAL TOTAL DISTANCE PER POINT
[129.01480904130943, 118.43486167034676, 175.62216974579803, 
115.00083356487914, 118.99999999999997, 128.00043196544277, 
135.0024697456162, 117.26026551404755, 115.01222901612005]
POINT PRODUCING MINIMAL TOTAL DISTANCE
3
DERIVATIVE OF LINE
16.9991664351
\end{verbatim}
\begin{center}
\includegraphics[width=\textwidth]{images/hw5p3.png}
\end{center}
One may think that this does not constitute a valid proof as to the validity of this choice of line, but calculating the maximum difference from attained result in-between the steps we get $error<stepsize*8*9$ (stepsize times the distance to furthest point is 8 multiplied by the total number of points), which with the standard value yields $error<0.72$ which is less than the second best distinct line. The best line of point 8 is in fact the same as the one of point 3. It can be proven analytically that there is a local minimum crossing these two lines. In this case, we assume that the line goes below points 0,3,7,8, crosses the points 1,4,5,6. Then we have the following expression for the total distance:
\begin{equation*}
\begin{split}
(-35+16+3a)-(-56+16+2a)+(0+16+a) \\
-(-3+16-a)-(4+16-2a)-(10+16-3a)\\
+(53+16-4a)+(69+16-5a)= 132-a
\end{split}
\end{equation*}
Where $13\leq a\leq 17$ due to the line being bounded to the stated conditions. Clearly, it is minimized at $a=17$ which yields the total distance $115$ with the derivative 17. The line going above point 8 is not permitted by conditions stated earlier. The same minimization argument can be applied to point 8. This local analytic solution of the minimum combined with the global bound produced numerically constitute a complete proof of the least total distance line for the given points.
\end{document}
\end{document}

from __future__ import division
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt

"""
This whole method follows the steps in 
http://docs.scipy.org/doc/scipy/reference/tutorial/linalg.html#solving-linear-system
where we want to find a,b,c for p*(x) = ax**2 + bx + c and x=xi0,xi1,xi2
"""
xi0 = sp.pi - sp.sqrt(2)*sp.pi/3
xi1 = sp.pi
xi2 = sp.pi + sp.sqrt(2)*sp.pi/3

func0 = sp.sin(sp.sqrt(2)*sp.pi/3) - sp.cos(sp.sqrt(2)*sp.pi/3)
func1 = -1
func2 = -sp.sin(sp.sqrt(2)*sp.pi/3) - sp.cos(sp.sqrt(2)*sp.pi/3)

coefmatr = np.array([[xi0**2, xi0, 1],[xi1**2, xi1, 1], [xi2**2, xi2, 1]])
ansarr = np.array([func0, func1, func2])

ans = np.linalg.solve(coefmatr,ansarr)

def minfunc(x):
    coefs = ans[:]
    return coefs[0]*x**2 + coefs[1]*x + coefs[2]
# Example code of how to run the function
if __name__ == "__main__":
    print ans
    xlist=np.linspace(sp.pi/3,5*sp.pi/3)
    plt.plot(xlist, (sp.cos(xlist) + sp.sin(xlist)))    
    plt.plot(xlist, minfunc(xlist))
    plt.legend(("f(x) = cos(x) + sin(x)","p(x) = {}x^2 - {}x + {}".format(round(abs(ans[0]),2), round(abs(ans[1]),2), round(abs(ans[2]),2))))
    plt.title("L1 approximation from the polynomials of degree 2 to f(x)")
    plt.show()
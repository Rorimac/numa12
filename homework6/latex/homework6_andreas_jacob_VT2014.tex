\documentclass[12pt,a4paper]{article}
\usepackage[utf8x]{inputenc}
\usepackage{ucs}
\usepackage{amsmath,amsthm}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\newtheorem{theorem}{Theorem}
\author{Andreas Söderlund \and Jacob Wikmark}
\title{Homework 6 \\ NUMA12}
\begin{document}
\maketitle
\section*{Task 1}
Here, we are constructing cardinal functions for three different local interpolation methods described in 18.1 in the book (Approximation theory and methods, M.J.D.Powell). The cardinal functions $l_k(x),\ x\in[a,b]$ define a basis that can be used to describe a curve 
\[
s(x) = \sum\limits_{k=0}^ml_k(x)f(x_k)
\]
that approximates a function $f\in\mathcal{C}[a,b]$ and satisfies $s(x_k) = f(x_k)$. This puts an extra condition on the basis functions: $l_k(x_i) = \delta_{ki}$. We also wish for the basis functions to be only locally nonzero (that is, that they only depend on a few of the data $x_k$) and that they consists of piecewise polynomials.

\paragraph{1}For the first local interpolation method, we are going to construct one of the cardinal functions for the piecewise linear interpolation. Piecewise linear interpolation is done by dividing the interval $[a,b]$ into intervals $\{x_i\leq x \leq x_{i+1}$ for $i\in\{0,1,\dots,m\}$ and $a=x_0<x_1<\dots<x_m=b$. We then define $s(x)$ as follows:
\[
s(x) = f(x_i) + \frac{x-x_i}{x_{i+1}-x{i}}(f(x_{i+1}-f(x_i)
\]
Following the book, we can construct $l_k(x)$ by using the function values $f(x_i) = \delta_{kj},\ j=0,\dots,m$ and using the previous definition for $l_k(x) = s(x)$. This was implemented in python using the following code:
\begin{verbatim}
def linearbase(datapts, centerpt, x):
    fval = np.zeros(len(datapts))
    fval[centerpt] = 1
    for i in range(len(datapts)):
        if x <= datapts[i]:
            return fval[i-1] + 
            ((x - datapts[i-1])/(datapts[i] - datapts[i-1]))*
            (fval[i]-fval[i-1])
    return "Error, x not in desired range"
\end{verbatim}
which we tried for the data points $x_i = i,\ i=-5, -4, \dots, 4, 5$ and $k = 0$ and plotted it using the following code:
\begin{verbatim}
datapts = np.linspace(-5,5,11)
xvals = np.linspace(-5,5,300)
y1vals = np.zeros(len(xvals))
for i in range(len(xvals)):
    y1vals[i] = linearbase(datapts,5,xvals[i])
plt.plot(xvals,y1vals)
plt.show()
\end{verbatim}
where we use $5$ because we want to access the 6:th element, that is, $k= 0$. From this we got the following graph:
\begin{center}
\includegraphics[width=\textwidth]{images/6_1_linear.png}
\end{center}
Piecewise linear interpolation is also how the \texttt{plot} function is implemented in the \texttt{matplotlib} library in python.
\paragraph{2}For the other two interpolation methods, piecewise cubic polynomials are used. In the first method, $s_i$ defined on the interval $x_i \leq x \leq x_{i+1}$ is a cubic polynomial defined by interpolating two more function values. If $1 \leq i \leq m-2$, the points $f(x_{i-1}),f(x_{i+2})$ are used in addition to $f(x_i)$ and $f(x_{i+1})$, otherwise other suitable values are used. The cubic polynomial is easily found by solving the linear equation system
\[
ax_j^3+bx_j^2+cx_j+d = f(x_j),\ j=i-1,\dots,i+2
\]
which was implemented in the following code:
\begin{verbatim}
def cubicsolv(xvals,yvals,x):
    if not np.any(yvals):
        return 0
    matr = np.zeros((4,4))
    for i in range(4):
        for j in range(4):
            matr[i,j] = xvals[i]**(3-j)
    coeffs = np.linalg.solve(matr,yvals)
    return coeffs[0]*x**3 + coeffs[1]*x**2 + coeffs[2]*x + coeffs[3]
\end{verbatim}
As in the previous case, to create the cardinal function we use the function values $f(x_i) = \delta_{kj},\ j=0,\dots,m$ and the following code to create the cardinal function.
\begin{verbatim}
def cubicbase1(datapts, centerpt, x):
    fval = np.zeros(len(datapts))
    fval[centerpt] = 1
    if x <= datapts[1]:
        return cubicsolv(datapts[:4],fval[:4],x)
    if x >= datapts[-2]:
        return cubicsolv(datapts[-4:],fval[-4:],x)
    for i in range(2,len(datapts)-2):
        if x <= datapts[i+1]:
            return cubicsolv(datapts[i-1:i+3],fval[i-1:i+3],x)
    return "Error, x not in desired range"
\end{verbatim}
Following the previous plot, we used \texttt{y2vals[i] = cubicbase1(datapts,5,xvals[i])} to generate the y-values for the plot.
\begin{center}
\includegraphics[width=\textwidth]{images/6_1_cubic1.png}
\end{center}
\paragraph{3}In the third method we impose conditions on the derivatives of the cubic polynomials instead of usin extra interpolation values as before. This has the positive effect of making the derivative of the spline continuous. In our case, we define the derivatives $s'(x_i)$ and $s'(x_{i+1})$ by the derivative of the quartic polynomial passing through the points $f(x_{i-2}),\dots,f(x_{i+2})$ at $x_i$ and $x_{i+1}$ respectively. To find the derivative of the quartic polynomial we used a similar method as the one to find the cubic polynomial in the following code:
\begin{verbatim}
def quartsolvder(xvals,yvals,x):
    matr = np.zeros((5,5))
    for i in range(5):
        for j in range(5):
            matr[i,j] = xvals[i]**(4-j)
    coeffs = np.linalg.solve(matr,yvals)
    return 4*coeffs[0]*x**3 + 3*coeffs[1]*x**2 + 
    2*coeffs[2]*x + coeffs[3]
\end{verbatim}
On each $x_i\leq x_{i+1}$ the curve is then defined as follows:
\[
s_i(x) = f(x_i) + s'(x_j)(x - x_i) + c_2(x-x_i)^2 + c_3(x - x_i)^3
\]
where
\[
c_2 = \frac{3(f(x_{i+1})-f(x_i))}{(x_{i+1} - x_i)^2} - \frac{2s'(x_i) + s'(x_{i+1})}{x_{i+1}-x_i}
\]
and
\[
c_3 = \frac{2(f(x_{i})-f(x_{i+1}))}{(x_{i+1} - x_i)^3} + \frac{s'(x_i) + s'(x_{i+1})}{(x_{i+1}-x_i)^2}
\]
Just as before, we use the function values $f(x_i) = \delta_{kj},\ j=0,\dots,m$ to create the cardinal function which was implemented in the code in the following way:
\begin{verbatim}
def cubicbase2(datapts, centerpt, x, diff = 1e-3):
    fval = np.zeros(len(datapts))
    fval[centerpt] = 1
    if x <= datapts[2]:
        return 0
    if x >= datapts[-3]:
        return 0
    for i in range(2,len(datapts)-3):
        if x <= datapts[i+1]:
            der1 = quartsolvder(datapts[i-2:i+3],fval[i-2:i+3],datapts[i])
            der2 = quartsolvder(datapts[i-1:i+4],fval[i-1:i+4],datapts[i+1])
            
            c2 = 3*(fval[i+1]-fval[i])/(datapts[i+1]-datapts[i])**2
             - (2.*der1 + der2)/(datapts[i+1]-datapts[i])
             
            c3 = 2*(fval[i]-fval[i+1])/(datapts[i+1]-datapts[i])**3
             + (der1 + der2)/(datapts[i+1]-datapts[i])**2
        
            return fval[i] + der1*(x - datapts[i])
             + c2*(x - datapts[i])**2 + c3*(x - datapts[i])**3
\end{verbatim}
and the y-values for the following plot used \texttt{y3vals[i] = cubicbase2(datapts,5,xvals[i])} just as before
\begin{center}
\includegraphics[width=\textwidth]{images/6_1_cubic2.png}
\end{center}
\section*{Task 2}
\begin{figure}[p]
\includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{images/hw6p2.png}
\caption{Approximation of function by 7 cubic splines using Chebyshev points as reference, $error=9.04\cdot 10^{-2}$}
\end{figure}
\begin{figure}[p]
\includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{images/hw6p22.png}
\caption{Approximation of function by 7 cubic splines using Chebyshev points with modified end points as reference, $error=5.81\cdot 10^{-2}$}
\end{figure}
We chose to use the Chebyshev points to approximate the function due to their close relationship to the function in question. A function calculating $n$ chebyshev points was constructed, and fed into the \emph{interp1d()}-function of python \emph{SciPy}. The sample points and the extra argument \emph{kind='cubic'} was used to construct cubic splines. This function takes as standard arguments the $x$-array, the $y$-array. Given at least $4$ data points, it will return a function of the cubic splines passing through the given points.This function will be defined on the interval from the smallest data point to the largest in terms of $x$. The resulting function was then evaluated numerically. Since the function \emph{interp1d} is only defined within the given interval, the end points $-1$ and $1$ needed to be included. This problem was solved by first running the function to evaluate the error, then the y-value of the end points was increased by half of the error (Increasing by less produced greater error, increasing by more gives higher than $50\%$ error at the end points). This reduced the error by almost $50\%$. The finder() function increases the number of sample points until the solution (the function which satisfies the given conditions) is found. This is highly inefficient and prone to error when run from the value 4; using 1000 sample points in the error evaluation gives a value of 384 as the number of splines required to perform the given task (find a cubic spline approximation with error$<10^{-3}$). Increasing the resolution of the error sampling to 10000 points, the error is greatly increased, and the actual solution is not found until 413 cubic splines. Further increasing the resolution to 100000 sample points did not yield any higher error, so 413 cubic splines (414 sample points) seems to be the least number of splines required to evaluate the function with a max error of $10^{-3}$. \\ \\
Below follows output of the final evaluations
\begin{verbatim}
>>>finder(10**(-3),410,True,100000)
Iteration:  411 Error:  0.00100583142294
Iteration:  412 Error:  0.00100328566033
Iteration:  413 Error:  0.00100076386752
Iteration:  414 Error:  0.000998277092524
Solution found at  414  sample points
producing  413  cubic splines
\end{verbatim}
Code for the functions used:
\begin{verbatim}
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
def cheby(n,inte=None):
    if inte==None:
        inte=[-1.,1.]
    radlist=np.linspace(np.pi,0,n)
    radi=(inte[1]-inte[0])/2
    center=inte[0]+radi
    px=[center+np.cos(r)*radi for r in radlist]
    py=[radi*np.sin(r) for r in radlist]
    return (px,py)

def retfunc(xlist,ylist):
    return interp1d(xlist,ylist,kind='cubic')

def stdfunc(n,offset=0):
    list=cheby(n)
    xlist=list[0]
    ylist=list[1]
    ylist[len(ylist)-1]+=offset
    ylist[0]+=offset
    return retfunc(xlist,ylist)

def funceval(func,plot=False,k=1000,inte=None):
    if inte==None:
        inte=[-1.,1.]
    radlist=np.linspace(np.pi,0,k)
    radi=(inte[1]-inte[0])/2
    center=inte[0]+radi
    px=[center+np.cos(r)*radi for r in radlist]
    maxerror=0
    for x in px:
        y=np.sin(np.arccos(x))
        error=abs(y-func(x))
        if error>maxerror:
            maxerror=error
    if plot==True:
        plt.plot(px,[np.sin(np.arccos(x)) for x in px],px,[func(x) for x in px])
        plt.show()
    return maxerror

def runner(n,printing=False,k=1000):
    func=stdfunc(n)
    err=funceval(func)
    if printing==True:
        print 'ORIGINAL ERROR'
        print err
    offset=err/2
    func=stdfunc(n,offset)
    err=funceval(func,False,k)
    if printing==True:
        print 'BOUNDARY VALUE COMPENSATED ERROR'
        print err
    return err

def finder(error,n=3,printing=False,evalsteps=1000):
    found=False
    while found==False:
        n+=1
        err=runner(n,False,evalsteps)
        if printing==True:
            print "Iteration: ", n, "Error: ",err
        if err<error:
            print "Solution found at ",n, " sample points"
            print "producing " ,n-1," cubic splines" 
            break

\end{verbatim}
\section*{Task 3}
Following the Schoenberg-Whitney theorem from the book we see that because we have the knots $\xi_i = j:\ j=-3,-2,\dots,12,13$ and the b-splines are $\{B_p:\ p=-3,-2,\dots,9$, $k$ must be equal to $3$. Using equation $(19.10)$ the b-splines are defined as follows:
\[
B_p(x) = \sum\limits_{j=p}^{p+4}\Big(\prod\limits_{i=p,i\neq j}^{p+4}\frac{1}{i - j}\Big)(x-j)_+^3
\]
where $p=-3,-2,\dots,9$ and $x\in [0,10]$.\\
Because the Schoenberg-Whitney theorem is satisfied, we have a unique solution to
\[
\sum\limits_{p=-3}^9 \lambda_p B_p(x_i) = f(x_i) = x_i^2
\]
if we use the evenly distributed points $x_i\ i=1,\dots,13$ where $x_1=0$ and $x_13=10$ as the numbers $B_{j-2}(x_j)$ are nonzero for $j=1,\dots,13$. This means that we only have to solve the following linear equation system
\[
\begin{pmatrix}
B_{-3}(x_1) & B_{-2}(x_1) & \cdots & B_{9}(x_{1} \\
B_{-3}(x_2) & B_{-2}(x_2) & \cdots & B_{9}(x_{2} \\
\vdots & \vdots & \ddots & \vdots \\
B_{-3}(x_13) & B_{-2}(x_13) & \cdots & B_{9}(x_{13}
\end{pmatrix}
\begin{pmatrix}
\lambda_{-3} \\
\lambda_{-2} \\
\vdots \\
\lambda_{9}
\end{pmatrix}
=
\begin{pmatrix}
x_1^2 \\
x_2^2 \\
\vdots \\
x_{13}^2
\end{pmatrix}.
\]
This can be easily done after we write a program that evaluates the b-splines. Using the definition of the b-spline of degree p described in theorem $19.3$ we wrote the following code to evaluate $B_p(x)$:
\begin{verbatim}
def b_sumbase(knots, degree, p, x):
    ans = 0
    for j in range(p,p+degree+2):
        mult = 1
        for i in range(p,p+degree+2):
            if i != j:
                mult = mult*(1/(knots[i]-knots[j]))
        signmult = (x - knots[j])**degree
        if signmult <= 0:
            signmult = 0
        ans = ans + mult*signmult
        
    return ans
\end{verbatim}
This means that we easily can create the matrix
\[
\mathbf{B} = 
\begin{pmatrix}
B_{-3}(x_1) & B_{-3}(x_2) & \cdots & B_{-3}(x_{13} \\
B_{-2}(x_1) & B_{-2}(x_2) & \cdots & B_{-2}(x_{13} \\
\vdots & \vdots & \ddots & \vdots \\
B_{9}(x_1) & B_{9}(x_2) & \cdots & B_{9}(x_{13}
\end{pmatrix}
\]
using the previous code. from this we can easily solve the linear equation system to get the values for the coefficients $\lambda_i$:
\begin{verbatim}
lambda_0 = 2.66666666666
lambda_1 = -1.33333333333
lambda_2 = 2.66666666667
lambda_3 = 14.6666666667
lambda_4 = 34.6666666667
lambda_5 = 62.6666666667
lambda_6 = 98.6666666667
lambda_7 = 142.666666667
lambda_8 = 194.666666667
lambda_9 = 254.666666667
lambda_10 = 322.666666667
lambda_11 = 398.666666667
lambda_12 = 482.666666667
\end{verbatim}
To calculate the b-spline curve we only need to write a function that evaluates the definition of the b-spline:
\[
s(x) = \sum\limits_{p}\lambda_p B_p(x)
\]
which we implemented using the following code:
\begin{verbatim}
def b_spline(coeffs,knots,degree,x):
    ans = 0
    for i in range(len(coeffs)):
        ans = ans + coeffs[i]*b_sumbase(knots,degree,i,x)
    return ans
\end{verbatim}
from this code we were able to plot our b-spline with the aforementioned coefficients to get the following graph:
\begin{center}
\includegraphics[width=\textwidth]{images/6_3_spline.png}
\end{center}
which surely seems like a god approximation to $x^2$. To check if this actually is a good approximation we look at the error for the points $x = 1/2,3/2\dots,17/2$ betweeb $x^2$ and our b-spline.
\begin{verbatim}
Difference for 0.5: 2.73114864058e-14
Difference for 1.5: 7.9936057773e-15
Difference for 2.5: 8.881784197e-16
Difference for 3.5: 2.13162820728e-14
Difference for 4.5: 3.5527136788e-15
Difference for 5.5: 6.39488462184e-14
Difference for 6.5: 9.23705556488e-14
Difference for 7.5: 2.13162820728e-14
Difference for 8.5: 7.1054273576e-14
\end{verbatim}
and we see that we have a good approximation in the interval $[0,10]$.
\end{document}

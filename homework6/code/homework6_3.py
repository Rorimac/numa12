# -*- coding: utf-8 -*-
from __future__ import division
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt

def b_0(knot1,knot2,u):
    """
    Used to create the basis functions using a recurrence relation.
    p is wich of the basis functions it is, u is the value to check
    """
    return (knot1 <= u < knot2)
    """
    if (knots[p] <= u < knots[p+1]):
        return 1
    else:
        return 0
    """

def b_sumbase(knots, degree, p, x):
    ans = 0
    for j in range(p,p+degree+2):
        mult = 1
        for i in range(p,p+degree+2):
            if i != j:
                mult = mult*(1/(knots[i]-knots[j]))
        signmult = (x - knots[j])**degree
        if signmult <= 0:
            signmult = 0
        ans = ans + mult*signmult
        
    return ans

def rec_rel(knots,degree,p,u):
    if degree <= 0:
        return b_0(knots[p],knots[p+1],u)
    return ((u - knots[p])*rec_rel(knots,degree-1,p,u)+(knots[p+degree+1]*rec_rel(knots,degree-1,p+1,u)))/(knots[p+degree+1]-knots[p])

def b_base(knots,degree,p,u):
    
    #If the degree is zero, return the zeroeth basis function.
    if degree <= 0:
        return b_0(knots[p],knots[p+1],u)
    
    return rec_rel(knots,degree,p,u)

def b_spline(coeffs,knots,degree,x):
    ans = 0
    for i in range(len(coeffs)):
        ans = ans + coeffs[i]*b_sumbase(knots,degree,i,x)
    return ans
    
if __name__ == "__main__":
    knots = sp.array([-3,-2,-1,0,1,2,3,4,5,6,7,8,9,10,11,12,13])
    """
    xvals = np.linspace(-3,13,300)
    def fastplt(xvals):
        return b_base(knots,3,i,xvals)
    yvals = np.zeros(len(xvals))
    for j in range(10):
        for i in range(len(yvals)):
            yvals[i] = b_sumbase(knots,3,j,xvals[i])
        plt.plot(xvals,yvals)
        
#    for i in range(0,13):
#       plt.plot(xvals,fastplt(xvals))
    plt.show()
    """
    xinterp = np.linspace(0,10,13)
    
    fval = xinterp**2
    
    bvals = sp.zeros((len(fval),len(fval)))
    for i in range(len(fval)):
        for j in range(0,13):
            bvals[i,j] = b_sumbase(knots,3,j,xinterp[i])
    print bvals
    
    coeffs = np.linalg.solve(bvals,fval)
    for i in range(len(coeffs)):
        print "lambda_{} = {}".format(i,coeffs[i])
    xvals = np.linspace(0,10,300)
    y1vals = xvals**2
    y2vals = np.zeros(len(xvals))
    for i in range(len(y2vals)):
        y2vals[i] = b_spline(coeffs,knots,3,xvals[i])
    
    xva = np.array([1/2,3/2,5/2,7/2,9/2,11/2,13/2,15/2,17/2])
    sqrva = xva**2
    bspva = np.zeros(len(xva))
    for i in range(len(xva)):
        bspva[i] = b_spline(coeffs,knots,3,xva[i])
    for i in range(len(xva)):
        print "Difference for {}: {}".format(xva[i],abs(sqrva[i]-bspva[i]))
#    plt.plot(xvals,y1vals)
    plt.plot(xvals,y2vals)
    plt.title("Plot of the b-spline for 0 < x < 10")
    plt.show()  
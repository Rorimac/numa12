# -*- coding: utf-8 -*-
from __future__ import division
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
    
def cubicsolv(xvals,yvals,x):
    if not np.any(yvals):
        return 0
    matr = np.zeros((4,4))
    for i in range(4):
        for j in range(4):
            matr[i,j] = xvals[i]**(3-j)
    coeffs = np.linalg.solve(matr,yvals)
    return coeffs[0]*x**3 + coeffs[1]*x**2 + coeffs[2]*x + coeffs[3]

def quartsolvder(xvals,yvals,x):
    matr = np.zeros((5,5))
    for i in range(5):
        for j in range(5):
            matr[i,j] = xvals[i]**(4-j)
    coeffs = np.linalg.solve(matr,yvals)
    return 4*coeffs[0]*x**3 + 3*coeffs[1]*x**2 + 2*coeffs[2]*x + coeffs[3]
    

def linearbase(datapts, centerpt, x):
    fval = np.zeros(len(datapts))
    fval[centerpt] = 1
    for i in range(len(datapts)):
        if x <= datapts[i]:
            return fval[i-1] + ((x - datapts[i-1])/(datapts[i] - datapts[i-1]))*(fval[i]-fval[i-1])
    return "Error, x not in desired range"

def cubicbase1(datapts, centerpt, x):
    fval = np.zeros(len(datapts))
    fval[centerpt] = 1
    if x <= datapts[1]:
        return cubicsolv(datapts[:4],fval[:4],x)
    if x >= datapts[-2]:
        return cubicsolv(datapts[-4:],fval[-4:],x)
    for i in range(2,len(datapts)-2):
        if x <= datapts[i+1]:
            return cubicsolv(datapts[i-1:i+3],fval[i-1:i+3],x)
    return "Error, x not in desired range"
    

def cubicbase2(datapts, centerpt, x, diff = 1e-3):
    fval = np.zeros(len(datapts))
    fval[centerpt] = 1
    if x <= datapts[2]:
        return 0
    if x >= datapts[-3]:
        return 0
    for i in range(2,len(datapts)-3):
        if x <= datapts[i+1]:
            der1 = quartsolvder(datapts[i-2:i+3],fval[i-2:i+3],datapts[i])
            der2 = quartsolvder(datapts[i-1:i+4],fval[i-1:i+4],datapts[i+1])
            
            c2 = 3*(fval[i+1]-fval[i])/(datapts[i+1]-datapts[i])**2 - (2.*der1 + der2)/(datapts[i+1]-datapts[i])
            c3 = 2*(fval[i]-fval[i+1])/(datapts[i+1]-datapts[i])**3 + (der1 + der2)/(datapts[i+1]-datapts[i])**2
        
            return fval[i] + der1*(x - datapts[i]) + c2*(x - datapts[i])**2 + c3*(x - datapts[i])**3
        
    

if __name__ == "__main__":
#    print quartsolvder([-2,-1,0,1,2],[0,0,1,0,0],1)
    datapts = np.linspace(-5,5,11)
    xvals = np.linspace(-5,5,300)
    
    y1vals = np.zeros(len(xvals))
    y2vals = np.zeros(len(xvals))
    y3vals = np.zeros(len(xvals))
#    print datapts
#    print xvals
    
    for i in range(len(xvals)):
        y1vals[i] = linearbase(datapts,5,xvals[i])
    plt.plot(xvals,y1vals)
    plt.title("Cardinal function for piecewise linear interpolation")
    plt.ylim( -2,2)
    plt.xlim(-5,5)
    plt.show()
    
    for i in range(len(xvals)):
        y2vals[i] = cubicbase1(datapts,5,xvals[i])
    plt.plot(xvals,y2vals)
    plt.title("Cardinal function for piecewise cubic interpolation")
    plt.ylim( -2,2)
    plt.xlim(-5,5)
    plt.show()
    
    for i in range(len(xvals)):
        y3vals[i] = cubicbase2(datapts,5,xvals[i])
    plt.plot(xvals,y3vals)
    plt.title("Cardinal function with continuous derivative")
    plt.ylim( -2,2)
    plt.xlim(-5,5)
    plt.show()
    
    
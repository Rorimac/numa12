import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
def cheby(n,inte=None):
    if inte==None:
        inte=[-1.,1.]
    radlist=np.linspace(np.pi,0,n)
    radi=(inte[1]-inte[0])/2
    center=inte[0]+radi
    px=[center+np.cos(r)*radi for r in radlist]
    py=[radi*np.sin(r) for r in radlist]
    return (px,py)

def retfunc(xlist,ylist):
    return interp1d(xlist,ylist,kind='cubic')

def stdfunc(n,offset=0):
    list=cheby(n)
    xlist=list[0]
    ylist=list[1]
    ylist[len(ylist)-1]+=offset
    ylist[0]+=offset
    return retfunc(xlist,ylist)

def funceval(func,plot=False,k=1000,inte=None):
    if inte==None:
        inte=[-1.,1.]
    radlist=np.linspace(np.pi,0,k)
    radi=(inte[1]-inte[0])/2
    center=inte[0]+radi
    px=[center+np.cos(r)*radi for r in radlist]
    maxerror=0
    for x in px:
        y=np.sin(np.arccos(x))
        error=abs(y-func(x))
        if error>maxerror:
            maxerror=error
    if plot==True:
        plt.plot(px,[np.sin(np.arccos(x)) for x in px],px,[func(x) for x in px])
        plt.show()
    return maxerror

def runner(n,printing=False,k=1000):
    func=stdfunc(n)
    err=funceval(func)
    if printing==True:
        print 'ORIGINAL ERROR'
        print err
    offset=err/2
    func=stdfunc(n,offset)
    err=funceval(func,False,k)
    if printing==True:
        print 'BOUNDARY VALUE COMPENSATED ERROR'
        print err
    return err

def finder(error,n=3,printing=False,evalsteps=1000):
    found=False
    while found==False:
        n+=1
        err=runner(n,False,evalsteps)
        if printing==True:
            print "Iteration: ", n, "Error: ",err
        if err<error:
            print "Solution found at ",n, " sample points"
            print "producing " ,n-1," cubic splines" 
            break
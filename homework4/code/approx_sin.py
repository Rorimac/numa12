import numpy as np  
import matplotlib.pyplot as plt
import remez

def fcn(x):
    return np.sin(x)

def chebyshev_nodes(degree, interval):
    """
    INPUT:   degree (positive integer), interval (as 2-tuple)\n
    OUTPUT:  List of Chebyshev nodes on the interval\n
    PURPOSE: To have a reference list to run the remez algorithm on\n
    METHOD:  Definition from wikipedia
    """
    
    #simple asertion that the input is correct
    assert type(degree) in [int,float]
    assert type(interval) in [list,tuple]
    assert len(interval) == 2 
    assert type(interval[0]) in [int,float] and type(interval[1]) in [int,float]
    assert interval[0]<interval[1]
    
    a,b = interval[0],interval[1]
    cheblist = []
    for i in range(1,degree+1):
        temp_x = (a + b)/2. - (b - a)/2.*np.cos(((2*i-1.)*np.pi)/(2*degree))
        cheblist.append(temp_x)
    return cheblist

def approx_fcn(x, degree, coefs):
    """
    INPUT:   x (real number) degree (positive integer), coefs (list)
    OUTPUT:  f(x)
    PURPOSE: create the function that approximates fcn(x)
    METHOD:  ass))
        plt.plot(xlist,ylist)umes that the basis functions are the monomial polynomials
    """
    assert type(degree) in [int,float]
    
    res = 0
    for i in range(degree):
        res = res + coefs[i]*x**i
    
    return res

def plot_approx(start,stop,interval,funct, tol = 1e-1):
    """
    INPUT:  start(integer, how many points to start with), stop (integer, how many points to stop with), interval (tuple), funct, a function dependent on x to be approximated
    
    """
    assert len(interval) == 2 
    assert type(interval[0]) in [int,float] and type(interval[1]) in [int,float]
    assert interval[0]<interval[1]
    
    fstring = "{}".format(funct)
    xlist=np.linspace(interval[0],interval[1])
    plt.plot(xlist,fcn(xlist))
    for i in range(start,stop+1):
        points = chebyshev_nodes(i + 1,interval)
        coefs = remez.remez('{}(x)'.format(fstring), interval, points, tol, plot = False)[0];
        ylist = []
        for x in xlist:
            ylist.append(approx_fcn(x, i, coefs))
        plt.plot(xlist,ylist)
    plt.title("The function and its approximations on the interval {}".format((round(interval[0],3),round(interval[1],3))))
    plt.legend(["function"]+ ["deg {}".format(i) for i in range(start,stop+1)])
    plt.show()
    
    #plot the error
    for i in range(start,stop+1):
        points = chebyshev_nodes(i + 1,interval)
        coefs = remez.remez('{}(x)'.format(fstring), interval, points, tol, plot = False)[0];
        ylist = []
        for x in xlist:
            ylist.append(abs(approx_fcn(x, i, coefs) - fcn(x))) # Uses the predetermined function as error check. VERY DIRTY! Should be fixed asap!
        plt.yscale('log')
        plt.plot(xlist,ylist)
    plt.title("The error of the approximation on the interval {}".format((round(interval[0],3),round(interval[1],3))))
    plt.legend(["deg {}".format(i) for i in range(start,stop+1)])
        
    plt.show()
    
    return

# Example code of how to run the function
if __name__ == "__main__":
    plot_approx(4,10,(-np.pi/2,np.pi/2),'np.sin')
    print ""
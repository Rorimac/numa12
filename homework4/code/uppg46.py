import numpy as np  
import matplotlib.pyplot as plt
import remez
import approx_sin

def uppg46():
    'INPUT:     N/A'
    'OUTPUT:    Result of approximation'
    'PURPOSE:   Solve 4.6, approximation of x^2 by 1st deg poly'
    'METHOD:    Remez exchange algorithm'
    f='np.square(x)'
    interval=[0.,1.]
    points=approx_sin.chebyshev_nodes(3,[0.,1.])
    print points
    tol=0.01
    outp=remez.remez(f,interval,points,tol,False)
    return outp[0]

if __name__ == "__main__":
    print uppg46()

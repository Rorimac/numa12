import numpy as np
import bisect
import matplotlib.pyplot as plt

def remez(f,interval,ref,tol,plot=None,nsp=None,A=None,auto=None,minit=None, minmax = False):
    'INPUT:     Function (function of x as string), interval (interval as 2-tuple), ref (initial reference values as list), tol (tolerance as positive scalar), plot (on or off as boolean), nsp (number of sample points, integer), A (Haar space basis as a list of strings),auto (automatic loop detection, boolean)'
    'OUTPUT:    coefs (polynomial coefficients as list), errest (error estimate as positive scalar), iterates (number of iterations, integer), hlist (reference errors as list), xilist (final reference values as list)'
    'PURPOSE:   Calculates minimax approximation'
    'METHOD:    Remez exchange algorithm'
    n=len(ref)-2
    
    'Setting default values'
    if plot==None:
        plot=True
    if nsp==None:
        nsp=10000
    if A==None:
        A=polyspace(n)
    if auto==None:
        auto=True
    if minit==None:
        minit=1
    autovar=True
    if minmax: autovar = False
    xlp=[]
    error=2*tol
    xilist=ref[:]
    iterates=0
    hlist=[]
    minmaxerror = []
    'Main loop'
    while (error>tol and autovar==True) or iterates<minit:
        iterates+=1
        coefs=coeff(A,xilist,f)
        hlist.append(coefs[n+1])
        'Creating approximation function string'
        func=''
        for deg in range(n+1):
            func=func+'+'+A[deg]+'*'+str(coefs[deg])
        'Finding maximum error with function maxdiff'
        err=maxdiff(f,func,interval,nsp)
        if minmax: minmaxerror = diffinder(f,func,interval,nsp)
        error=err[0]
        j=bisect.bisect_left(xilist,err[1])-1
        if j>-1:
            xlow=xilist[j]
            x=xlow
            errlow=eval(f)-eval(func)
            signlow=np.sign(errlow)
            if signlow==err[2] or j>n:
                xilist[j]=err[1]
            else:
                xilist[j+1]=err[1]
        else:
            xilist[j+1]=err[1]
        if plot:
            plotter(f,func,interval)
        if auto==True:
            if xlp==xilist:
                autovar=False
        xlp=xilist[:]
    errest=coefs.pop()
    'Printing results for easier viewing'
    print 'COEFFICIENTS:\t\t',coefs
    print 'ERROR ESTIMATE:\t\t', errest
    print 'ITERATIONS:\t\t', iterates
    print 'REFERENCE ERROR:\t',hlist
    print 'REFERENCE VALUES:\t',xilist
    print '-------------'
    return (coefs,errest,iterates,hlist,xilist,minmaxerror)

def maxdiff(func1,func2,interval,nsp):
    'INPUT:     func1, func2 (functions of x to be compared as strings), interval (the interval as 2-tuple),nsp (number of sample points, integer)'
    'OUTPUT:    mdif (maximum absolute error, scalar), difpoint (point of maximum absolute error), sign (sign of func1-func2 at max error point, +1 or -1)'
    'PURPOSE:   Calculating inf-norm of the difference of two functions'
    'METHOD:    Sampling'
    xvals=list(np.linspace(interval[0],interval[1],nsp))
    dif=[]
    for x in xvals:
        dif.append(eval(func1)-eval(func2))
    abslist=[abs(x) for x in dif]
    mdif=max(abslist)
    mdifpos=abslist.index(mdif)
    x=xvals[mdifpos]
    sign=np.sign(dif[mdifpos])
    difpoint=interval[0]+(interval[1]-interval[0])/nsp*mdifpos
    return (mdif,difpoint,sign)
    
def diffinder(func1, func2, interval,nsp ):
    xvals=list(np.linspace(interval[0],interval[1],nsp))
    dif=[]
    max_x = 0
    min_x = 0
    min_y = 0
    max_y = 0
    for x in xvals:
        difference = eval(func1)-eval(func2)
        dif.append(difference)
        if max(dif) <= difference:
            max_x = x
            max_y = difference
        if min(dif) >= difference:
            min_x = x
            min_y = difference
    return (min_x,max_x,min_y,max_y)

def polyspace(n):
    'INPUT:     n (positive integer)'
    'OUTPUT:    poly (list of strings)'
    'PURPOSE:   Creating basis for P^(n) in strings'
    'METHOD:    String operations'
    poly=[]
    for k in range(n+1):
        poly.append('x**'+str(k))
    poly[0]='1.'
    return poly

def coeff(A,xilist,f):
    'INPUT:     A (Basis as list of strings), xilist (reference values as list), f (function of x as string)'
    'OUTPUT:    M (matrix)'
    'PURPOSE:   Calculates coefficients for algorithm'
    'METHOD:    As described in literature'
    n=len(A)
    'Calculates coefficient matrix'
    M=[]
    for k in range(n+1):
        Mtemp=[]
        x=xilist[k]
        for l in range(n):
            Mtemp.append(eval(A[l]))
        Mtemp.append((-1.)**k)
        M.append(Mtemp)
    'Calculates function array'
    fout=[]
    for x in xilist:
        fout.append(eval(f))
    return list(np.linalg.solve(M,fout))

def plotter(func1,func2,interval):
    'INPUT:     func1, func2 (functions of x as strings), interval (2-tuple)'
    'OUTPUT:    None'
    'PURPOSE:   Plots two functions on the given interval'
    'METHOD:    Evaluation at points'
    xlist=np.linspace(interval[0],interval[1])
    ylist1=[]
    ylist2=[]
    for x in xlist:
        ylist1.append(eval(func1))
        ylist2.append(eval(func2))
    plt.plot(xlist,ylist1)
    plt.plot(xlist,ylist2)
    plt.legend(("Function","Estimate"))
    plt.show()
    return 

# Example code of how to run the function
if __name__ == "__main__":
    def funct(x):
        return np.sin(x)
    
    remez('funct(x)',(-np.pi/2,np.pi/2),[-3,-2,0.,1,1.14],1e-2)
    print("")

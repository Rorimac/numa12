################
# Not working for 4.6!
# Should not use as is!
#
################
import numpy as np  
import matplotlib.pyplot as plt
import remez
import approx_sin

def function_x(x):
    return x**2

# This is almost identical to the plot approx from approx_sin but with changed fcn to function_x
def plot_approx(start,stop,interval,funct, tol = 1e-1):
    """
    INPUT:  start(integer, how many points to start with), stop (integer, how many points to stop with), interval (tuple), funct, a function dependent on x to be approximated
    
    """
    assert len(interval) == 2 
    assert type(interval[0]) in [int,float] and type(interval[1]) in [int,float]
    assert interval[0]<interval[1]
    
    fstring = "{}".format(funct)
    xlist=np.linspace(interval[0],interval[1])
    plt.plot(xlist,function_x(xlist))
    for i in range(start,stop+1):
        points = approx_sin.chebyshev_nodes(i + 1,interval)
        coefs = remez.remez('{}(x)'.format(fstring), interval, points, tol, plot = False)[0];
        ylist = []
        for x in xlist:
            ylist.append(approx_sin.approx_fcn(x, i, coefs))
        plt.plot(xlist,ylist)
    plt.title("The function and its approximations on the interval {}".format((round(interval[0],3),round(interval[1],3))))
    plt.legend(["function"]+ ["deg {}".format(i) for i in range(start,stop+1)])
    plt.show()
    
    #plot the error
    for i in range(start,stop+1):
        points = approx_sin.chebyshev_nodes(i + 1,interval)
        coefs = remez.remez('{}(x)'.format(fstring), interval, points, tol, plot = False)[0];
        ylist = []
        for x in xlist:
            ylist.append(abs(approx_sin.approx_fcn(x, i, coefs) - function_x(x))) # Uses the predetermined function as error check. VERY DIRTY! Should be fixed asap!
        plt.yscale('log')
        plt.plot(xlist,ylist)
    plt.title("The error of the approximation on the interval {}".format((round(interval[0],3),round(interval[1],3))))
    plt.legend(["deg {}".format(i) for i in range(start,stop+1)])
        
    plt.show()
    
    return

# Example code of how to run the function
if __name__ == "__main__":
    plot_approx(4,10,(-np.pi/2,np.pi/2),'np.square')
    print ""
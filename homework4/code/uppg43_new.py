import numpy as np
import matplotlib.pyplot as plt
import remez


def texout(iterations=5):
    """
    This function tries to mimic the behaviour of the example 8.3 from M.J.D.Powell Approximation theory and methods
    """
    func = 'np.sin(x)'
    basis = ['x', 'x**2']
    plot = False
    interval = (0, np.pi/2)
    tolerance = 1e-4
    reference = [0.5, 1., np.pi/2]
    itref = reference[:]
    
    tex1string = r"\begin{tabular}{ l l l l }\hline\hline Iteration & $\xi_0$ & $\xi_1$ & $\xi_2$ & $h(\xi_0,\xi_1,\xi_2)$ \\ \hline "
    tex2string = r"\begin{tabular}{ l l l l }\hline\hline Iteration & $\eta_0$ & $e(\eta_0$ & $\eta_1$ & $e(\eta_1)$ \\ \hline "

    for i in range(1, iterations + 1):
        coefs, errest, iterates, hlist, xilist,extrema = remez.remez(func, interval, reference, tolerance, plot, nsp = None, A = basis, auto = True, minit = i)
        tex1string = tex1string + r" ${it}$ & ${xi0}$ & ${xi1}$ & ${xi2}$ & ${h}$ \\ ".format(it = iterates, xi0 = itref[0], xi1 = itref[1], xi2 = itref[2], h = errest)
        itref = xilist[:]
        tex2string = tex2string + r" ${it}$ & ${eta0}$ & ${eeta0}$ & ${eta1}$ & ${eeta1}$ \\ ".format(it =iterates, eta0 = extrema[0], eeta0 = extrema[1], eta1 = extrema[2], eeta1 = extrema[3])
        
        

    tex1string = tex1string + r"\hline\hline \end{tabular}"        
    tex2string = tex2string + r"\hline\hline \end{tabular}" 
    
    with open('uppg43_reftable.tex','w') as file1:
        file1.write(tex1string)
        
    with open('uppg43_exttable.tex','w') as file2:
        file2.write(tex2string)

    return 0

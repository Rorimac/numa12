import numpy as np  
import matplotlib.pyplot as plt
import remez

def fcn(x):
    return np.sin(x)

def chebyshev_nodes(degree, interval):
    """
    INPUT:   degree (positive integer), interval (as 2-tuple)\n
    OUTPUT:  List of Chebyshev nodes on the interval\n
    PURPOSE: To have a reference list to run the remez algorithm on\n
    METHOD:  Definition from wikipedia
    """
    
    #simple asertion that the input is correct
    assert type(degree) in [int,float]
    assert type(interval) in [list,tuple]
    assert len(interval) == 2 
    assert type(interval[0]) in [int,float] and type(interval[1]) in [int,float]
    assert interval[0]<interval[1]
    
    a,b = interval[0],interval[1]
    cheblist = []
    for i in range(1,degree+1):
        temp_x = (a + b)/2. - (b - a)/2.*np.cos(((2*i-1.)*np.pi)/(2*degree))
        cheblist.append(temp_x)
    return cheblist

def approx_fcn(x,coefs,A):
    res=0
    for i in range(len(coefs)):
        res+=coefs[i]*eval(A[i])
    return res

def plot_approx(start,stop,interval,funct, tol=None):
    if tol==None:
        tol=0.5
    """
    INPUT:  start(integer, how many points to start with), stop (integer, how many points to stop with), interval (tuple), funct, a function dependent on x to be approximated
    
    """
    assert len(interval) == 2 
    assert type(interval[0]) in [int,float] and type(interval[1]) in [int,float]
    assert interval[0]<interval[1]
    
    fstring = "{}".format(funct)
    xlist=np.linspace(interval[0],interval[1])
    plt.plot(xlist,fcn(xlist))
    for i in range(start,stop+1):
        A=oddpolyspace(i-1)
        points = chebyshev_nodes(i + 1,interval)
        print '{}(x)'.format(fstring), interval, points, tol, False,None,A
        coefs = remez.remez('{}(x)'.format(fstring), interval, points, tol, False,None,A,None,5)[0];
        ylist = []
        for x in xlist:
            ylist.append(approx_fcn(x, coefs,A))
        plt.plot(xlist,ylist)
    plt.title("The function and its approximations on the interval {}".format((round(interval[0],3),round(interval[1],3))))
    plt.legend(["function"]+ ["dim {}".format(i) for i in range(start,stop+1)])
    plt.show()
    
    #plot the error
    for i in range(start,stop+1):
        points = chebyshev_nodes(i + 1,interval)
        A=oddpolyspace(i-1)
        coefs = remez.remez('{}(x)'.format(fstring), interval, points, tol, False,None,A,None,5)[0];
        ylist = []
        for x in xlist:
            ylist.append(abs(approx_fcn(x, coefs,A) - fcn(x))) # Uses the predetermined function as error check. VERY DIRTY! Should be fixed asap!
        plt.yscale('log')
        plt.plot(xlist,ylist)
    plt.title("The error of the approximation on the interval {}".format((round(interval[0],3),round(interval[1],3))))
    plt.legend(["dim {}".format(i) for i in range(start,stop+1)])
        
    plt.show()
    
    return

def oddpolyspace(n):
    A=remez.polyspace(2*n+1)
    polyodd=[A[2*x+1] for x in range(n+1)]
    return polyodd

# Example code of how to run the function
if __name__ == "__main__":
    plot_approx(1,10,(0.,np.pi/2),'np.sin')
    print ""

from __future__ import division
import scipy as sp
import numpy as np
import pylab as pl
from scipy.special import binom as bin
import scipy.optimize

def l1(x):
    if 0 <= x <= 1:
        return (x - 1.02)*(x - 1.04)*(x - 1.05)/((1 - 1.02)*(1 - 1.04)*(1 - 1.05))
    else: return 0
    
def l2(x):
    if 0 <= x <= 1:
        return (x - 1.)*(x - 1.04)*(x - 1.05)/((1.02 - 1)*(1.02 - 1.04)*(1.02 - 1.05))
    else: return 0
    
def l3(x):
    if 0 <= x <= 1:
        return (x - 1.)*(x - 1.02)*(x - 1.05)/((1.04 - 1.)*(1.04 - 1.02)*(1.04 - 1.05))
    else: return 0
    
def l4(x):
    if 0 <= x <= 1:
        return (x - 1.)*(x - 1.02)*(x - 1.04)/((1.05 - 1)*(1.05 - 1.02)*(1.05 - 1.04))
    else: return 0
    
def l5(x):
    if 0 <= x <= 1:
        return (x - 1.05)/((1. - 1.05))
    else: return 0

def l6(x):
    if 0 <= x <= 1:
        return (x - 1.)/((1. - 1.05))
    else: return 0
    
def abssum(x):
    return abs(l1(x)) + abs(l2(x)) + abs(l3(x)) + abs(l4(x))
    
def abssum2(x):
    return abs(l5(x)) + abs(l6(x))
    
if __name__ == "__main__":
#    print sp.optimize.fmin(lambda x: -abssum(x), 0)
#    print ""
#    print abssum(0)
    print sp.optimize.fmin(lambda x: -abssum2(x), 0)
    print ""
    print abssum2(0)
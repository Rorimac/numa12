from __future__ import division
import scipy as sp
import numpy as np
import pylab as pl
import scipy.interpolate
from scipy.special import binom as bin

def testfcn(x):
    return 10*np.exp(-3*x)
    
def function(x):
    return 3*x*sp.exp(x) - sp.exp(2*x)
    
def divided_differences(xvalues, func):
    """
    calculates the divided differences as defined in the book Approx theory and methods by Powell page 50
    """
    difflist = [func(xvalues)]
    for i in range(1,len(xvalues)):
        tmparray = np.zeros(len(xvalues)-i)
        for j in range(len(tmparray)):
            tmparray[j] = (difflist[i-1][j+1] - difflist[i-1][j])/(xvalues[j + i] - xvalues[j])
        difflist.append(tmparray)
    return difflist

def lagrange_approx(x, xvalues, func):
    """
    Calculates the lagrange approximation at a point using divided differences and the formula from p. 49
    """
    difflist = divided_differences(xvalues, func)
    res = difflist[0][0]
    for i in range(1,len(xvalues)):
        exes = 1
        for j in range(i):
            exes = exes*(x - xvalues[j])
        res = res + exes*difflist[i][0]
    return res
    

if __name__ == "__main__":
#    print divided_differences(np.array([1.6, 1.63, 1.7, 1.76, 1.8]), testfcn)
    print lagrange_approx(1.03, np.array([1., 1.02, 1.04, 1.05]), function)
    """
    xvalues = np.array([1., 1.02, 1.04, 1.05])
    lag = sp.interpolate.lagrange(xvalues, function(xvalues))
    print lag(1.03)
    print function(1.03)
    """
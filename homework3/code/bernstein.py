from __future__ import division
import scipy as sp
import numpy as np
import pylab as pl
from scipy.special import binom as bin

def function(x):
    return sp.absolute(x-1./2)

def bernstein_approx(degree, x):
    summ = 0
    for i in range(int(degree)):
	summ = summ + bin(degree,i)*(x**i)*((1-x)**(degree-i))*function(i/degree)
    return summ

# implements the Bernstein approximation operator as defined in the book
def bernstein_plot(degree, plotpts):
    """
    This function is used to approximate a function on the interval [0,1]. The degree is the degree of the Bernstein polynomial nad plotpnts is the number of points on the interval to plot.
    """

    xvalues = np.linspace(0, 1, num=plotpts)
    yvalues = np.zeros(plotpts)
    for i in range(plotpts):
        yvalues[i] = bernstein_approx(degree, xvalues[i])
    print 
    error = 0
    for i in range(len(xvalues)):
        err = (yvalues[i] - function(xvalues[i]))
        if abs(err)>= error:
            error = err 
    print error
    pl.plot(xvalues, function(xvalues))
    pl.plot(xvalues,yvalues)
    pl.xlabel("x",fontsize=16)
    pl.ylabel("y", fontsize=16)
    pl.title("Approximation of the function using Bernstein polynomials of degree {}".format(degree))
    pl.show()

if __name__ == "__main__":
    bernstein_plot(1000,200)